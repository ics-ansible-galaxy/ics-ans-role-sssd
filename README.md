ics-ans-role-sssd
=================

Ansible role to install SSSD (System Security Services Daemon).

Requirements
------------

- ansible >= 2.7
- molecule >= 2.6

Role Variables
--------------

Windows AD
```yaml
sssd_disabled: False
sssd_ldap_domain: esss.lu.se
sssd_ldap_uri: ldap://esss.lu.se
sssd_ldap_search_base: dc=esss,dc=lu,dc=se
sssd_ldap_default_bind_dn: cn=ldapreadonly,cn=Users,dc=esss,dc=lu,dc=se
sssd_ldap_default_authtok: password
```

OpenLdap
```yaml
sssd_ldap_default_authtok: SomePass
sssd_ldap_default_bind_dn: "uid=ABot,ou=Service accounts,dc=esss,dc=lu,dc=se"
sssd_ldap_uri: ldap://ldap.ics.esss.lu.se
sssd_ldap_schema: rfc2307
sssd_ldap_id_mapping: "False"
sssd_ldap_referrals: "True"
sssd_limit_groups:  # optional parameter, this limits access for users in the following group
  - cn=group1,ou=groups,dc=esss,dc=lu,dc=se
  - cn=group2,ou=groups,dc=esss,dc=lu,dc=se

To manage POSIX group from our openldap server, define these 2 variables:
sssd_group_objectclass:
sssd_group_search_base:

Use "sssd_ldap_schema: rfc2307 " if you use PosixGroup

```

Set `sssd_disabled` to True to disable SSSD.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sssd
```

License
-------

BSD 2-clause
